# This project is deprecated.
### Please refer to https://gitlab.oit.duke.edu/OIT-DCC/apptainer-example

# Singularity Container Example

This example project gives the steps needed to create a Singularity container for use in the Duke Compute Cluster (DCC) or elsewhere.

* Create a project in GitLab.
    * If you don't have an account on https://gitlab.oit.duke.edu, going to the URL in your browser and clicking on the "Duke Shibboleth Login" button will automatically log you in and create an account for you.
    * If you're working in a research group you should create a GitLab group so that team members can use share your projects. If another team member has already created a group, once you have an account have them add you as a member. The person to create a group owns it, but can add other users as members and/or grant privileges such developer or master to the group. If this is a standalone project you can just create it under your user name.
    * As a point of information, the name of the project you create will also be the name of your Singularity container image.

* Create a _Singularity.def_ file and add it to your project. The example in the _OIT-DCC/singularity-example_ project uses a Docker image of Anaconda Python 3 with the tensor flow module installed. 
* Create a _.gitlab-ci.yml_ file to run the Continuous Integration pipeline. The contents of the file should match the contents of the _.gitlab-ci.yml_ file in the _OIT-DCC/singularity-example_ project. Once this file is added to your project every time you push a change to your project a CI build of your Singularity image will be done and the container results placed on the research-singularity-registry server that will allow you to use wget or curl to download the compiled image file. The example _.gitlab-ci.yml_ file has a commented out line that also pushes an OCI formatted copy of the compiled container to the projects container registry. If you will be using this uncomment the last line that does the push. If you want to use your projects container registry you will need enable the container registry in your projects settings.

* The Singularity images created are stored on research-singularity-registry.oit.duke.edu. You can pull your image down to the DCC by logging into one of the DCC login servers (dcc-login-01/dcc-login-02/dcc-login-03) and pulling your image down by _curl_ or _wget_ (_e.g. curl -O https://research-singularity-registry.oit.duke.edu/OIT-DCC/singularity-example.sif_).

* If you need to pull your image into the PACE environment use the following steps:
  * Set proxy variables for PACE
    * export http_proxy=http://pacegateway.dhe.duke.edu:3128/
    * export https_proxy=https://pacegateway.dhe.duke.edu:3128/
  * Download your image using public facing server
    * wget --no-check-certificate https://research-singularity-registry-public.oit.duke.edu/OIT-DCC/singularity-example.sif

* To use the image you would include ""singularity exec"" or "singularity run", whichever your Singularity.def file has set up in your submit scripts. For example:
``` YAML
# !/Bin/Bash
# SBATCH -E slurm.err
singularity exec {your project}.sif some_script.py
```


